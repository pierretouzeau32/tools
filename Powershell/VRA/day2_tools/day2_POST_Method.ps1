### Template script pour action day2 depuis powershell en utilisant Invoke-vRARestMethod ###
######

#### Avant de lancer le script / utiliser la fonction, vérifier la présence d'une connexion vra active ($vRAConnection)

Function action_day2($exemple){
# Récupération du ressource ID (si utilisation d'une vm dans la day2):

$resource = Get-vRAResource -Name $exemple
$resource_id = $resource.ResourceId
Write-Host $resource_id -ForegroundColor Cyan

# URL de l'api (changer action id selon l'id de notre api)
$uri = "/catalog-service/api/consumer/resources/$resource_id/actions/{{ action id }}/requests"

# Path de notre fichier json contenant le "body" de la requête
$jsonfile = Get-Content "path_fichier_json.json"

# Configuration des paramètres
$Params = @{
	Method = "POST"
	Uri = $uri
	Body = [string]$jsonfile
}

# Utilisation de Invoke-vRARestMethod (possible de forcer l'utilisation de webrequest au lieu de rest en mettant -WebRequest)
$response = Invoke-vRARestMethod @Params 

}
