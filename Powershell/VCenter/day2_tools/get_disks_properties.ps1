# Recover information from the VM disks | > only use name of VM
# Need to be connected to VI Server
Function get_disks_properties ($vm) {

    $VMView = Get-View -ViewType VirtualMachine -Filter @{"Name" = $vm}
    $Disks = Get-HardDisk -VM $vm

    Foreach ($Disk in $Disks) {
				$DiskProps = New-Object PsObject
				$DiskProps | Add-Member Noteproperty VMName -Value $_.Name
				$DiskProps | Add-Member Noteproperty "Disk Name" -Value $Disk.Name
				$Info = $VMView.Config.Hardware.Device | where {$_.GetType().Name -eq "VirtualDisk"} | where {$_.DeviceInfo.Label -eq $Disk.Name}
				$DiskProps | Add-Member Noteproperty "SCSI Controller" -Value $Info.ControllerKey
				$DiskProps | Add-Member Noteproperty "SCSI ID" -Value $Info.UnitNumber
				$DiskProps | Add-Member Noteproperty Persistence -Value $Disk.Persistence
                $DiskProps
    }

}
