# Tools



## Infos

<img src="https://badges.frapsoft.com/os/v1/open-source.png?v=103">

Ce repo contiendra des bouts de code permettant de faire certaines fonctions, il me servira à gagner du temps etc.. 
Pour cette raison il n'y aura pas de script complet ainsi que la raison de la confidentialité de mon client.

Le repo contiendra une grosse partie powershell avec vRa, vRops, vRo car je travaille dessus le plus souvent, mais une partie GoLang (en formation), Ansible (en formation), GCP et Shell arriveront dans futur proche.

## Powershell:

<img src="https://img.shields.io/badge/Powershell-2CA5E0?style=for-the-badge&logo=powershell&logoColor=white">

1. vRa:
  * day2post

## GoLang:

<img src="https://img.shields.io/badge/Go-00ADD8?style=for-the-badge&logo=go&logoColor=white">

## Ansible:

<img src="https://img.shields.io/badge/ansible-%231A1918.svg?style=for-the-badge&logo=ansible&logoColor=white">

1. Formation:
  * get_log_moyenne.yml



## GCP:

<img src="https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white">

## Shell:

<img src="https://img.shields.io/badge/Shell_Script-121011?style=for-the-badge&logo=gnu-bash&logoColor=white">
